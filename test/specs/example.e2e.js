const LoginPage = require('../pageObjects/login.page');
const EventPage = require('../pageObjects/event.page');

describe('My Test', () => {
  it('Checking the form for creating a new event', async () => {
    await LoginPage.open();
    await (await LoginPage.btnLogin).click();
    await LoginPage.login('trandrej26@gmail.com', 'qwerty!123');
    await EventPage.open();
    await (await EventPage.naviBtnEvent).click();
    await browser.pause(1000);
    await (await EventPage.btnCreateEvent).click();
    await browser.pause(1000);
    await (await EventPage.btnNext).click();
    await browser.pause(1000);
    await (await EventPage.inputNameEvent).scrollIntoView();
    await expect(EventPage.fieldError).toHaveTextContaining('обязательно для заполнения.');
    await browser.pause(1000);
    await (await EventPage.inputNameEvent).setValue('Тестовое мероприятие');
    await browser.pause(1000);
    await (await EventPage.btnNext).click();
    await browser.pause(1000);
    await (await EventPage.btnOnlineEvent).click();
    await browser.pause(1000);
    await EventPage.createDataAndTimeEvent('20.11.2022', '16.00', '16.30');
    await browser.pause(1000);
    await (await EventPage.fieldLink).setValue('test.test');
    await browser.pause(1000);
    await (await EventPage.btnNext).click();
    await browser.pause(1000);
    await (await EventPage.btnPush).click();
    await browser.pause(1000);
    await (await EventPage.btnPushInModal).click();
    await browser.pause(1000);
    await expect(EventPage.messageResult).toHaveTextContaining(
      'Ваше мероприятие отправлено на модерацию\nПосле проверки администраторы опубликуют его в открытом доступе.',
    );
  });
});
