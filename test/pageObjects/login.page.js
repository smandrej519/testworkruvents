const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
  /**
     * define selectors using getter methods
     */
  get inputUsername() {
    return $('[dataqa="loginEmail"] .app-input__inner');
  }

  get inputPassword() {
    return $('[data-qa="loginPassword"] .app-input__inner');
  }

  get btnSubmit() {
    return $('[data-qa="loginSubmitBtn"]');
  }

  get btnLogin() {
    return $('[data-qa="loginOpenBtn"]');
  }

  get capcha() {
    return $('[data-testid="checkbox-captcha"]');
  }

  get captchaContainer() {
    return $('[data-testid="smartCaptcha-container"]');
  }

  get iframe() {
    return $('[data-testid="checkbox-iframe"]');
  }

  get messageErrorModal() {
    return $('.el-notification__group');
  }

  /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
  async login(username, password) {
    await this.inputUsername.setValue(username);
    await this.inputPassword.setValue(password);
    await this.btnSubmit.click();
    await this.captchaContainer.click();
    await this.iframe.waitForExist();
    browser.switchToFrame(this.iframe);
    // await this.capcha.click();
    await browser.pause(1000);
    browser.switchToParentFrame();
    await this.btnSubmit.click();
    await browser.pause(2000);
  }

  /**
     * overwrite specific options to adapt it to page object
     */
  open() {
    return super.open();
  }
}

module.exports = new LoginPage();
