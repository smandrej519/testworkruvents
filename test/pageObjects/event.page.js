const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class EventPage extends Page {
  /**
     * define selectors using getter methods
     */
  get naviBtnEvent() {
    return $('nav.header-nav .nuxt-link-active');
  }

  get btnCreateEvent() {
    return $('[data-qa="eventCreateBtn"]');
  }

  get inputNameEvent() {
    return $('input[data-qa="eventName"]');
  }

  get btnNext() {
    return $('[data-qa="eventNextBtn"]');
  }

  get messageErrorModal() {
    return $('.el-notification__group');
  }

  get fieldError() {
    return $('[data-qa="eventName"] .app-validate.is-error');
  }

  get btnOnlineEvent() {
    return $('h4=Онлайн-мероприятие');
  }

  get fieldStartDate() {
    return $('[placeholder="Дата начала"] .app-input__inner');
  }

  get fieldStartTime() {
    return $('[placeholder="Время начала"] .app-input__inner');
  }

  get fieldEndTime() {
    return $('[placeholder="Время окончания"] .app-input__inner');
  }

  get fieldLink() {
    return $('[placeholder="Ссылка"] .app-input__inner');
  }

  get btnPush() {
    return $('[data-qa="eventPublishBtn"]');
  }

  get btnPushInModal() {
    return $('[data-qa="eventPublishModal"] .app-dialog__button');
  }

  get messageResult() {
    return $('.draft-action-bar__left');
  }

  async createDataAndTimeEvent(startDate, startTime, endTime) {
    await this.fieldStartDate.setValue(startDate);
    await this.fieldStartTime.setValue(startTime);
    await this.fieldEndTime.setValue(endTime);
  }

  open() {
    return super.open('events');
  }
}

module.exports = new EventPage();
